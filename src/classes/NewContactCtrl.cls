public with sharing class NewContactCtrl {
	public Contact record {get; set;}
	public String errorMessage {get; set;}
	
	public NewContactCtrl() {
		this.record = new Contact();
		this.errorMessage = '';
	}
	
	public PageReference doSave() {
		List<Contact> contacts = [
			SELECT Email 
			FROM Contact 
			WHERE Email = :this.record.Email 
			LIMIT 1];
		
		if ( ! contacts.isEmpty()) {
			this.errorMessage = 'Contact already exist : <a href="' + URL.getSalesforceBaseUrl().toExternalForm() + 
					'/' + contacts.get(0).Id + '">' + URL.getSalesforceBaseUrl().toExternalForm() + 
					'/' + contacts.get(0).Id + '</a>';
			
			return null;
		} else {
			try {
				this.errorMessage = '';
				insert this.record;
			} catch(Exception ex) {
				this.errorMessage = ex.getMessage();
				
				return null;
			}
		}
		
		return new PageReference('/' + this.record.Id);
	}
}