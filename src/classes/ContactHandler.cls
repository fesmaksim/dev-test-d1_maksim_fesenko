/**
* @description Contact Handler class for managing Contact actions.
*/
public with sharing class ContactHandler {
	//This variable needs for switching off contact trigger by Apex code if it's need.
	public static Boolean enablesTrigger = true;
	
	public static final Map<String, String> PRIORITY_LEVEL = new Map<String, String> {
		'Primary' => 'High', 
		'Secondary' => 'Medium', 
		'Tertiary' => 'Low'
	};
	
	public static final Map<String, Date> ACTIVITY_DATE = new Map<String, Date> {
		'High' => Date.today().addDays(7), 
		'Medium' => Date.today().addDays(14), 
		'Low' => Date.today().addDays(21)
	};
	
	/**
	* @decsription Filling Case fields when it will attach to Contact on insert action.
	*
	* @param p_newRecords This is trigger.new records. Type of param is List<Contact>.
	*/
	public static void attachCase(List<Contact> p_newRecords) {
		Set<Id> accountIds = new Set<Id>();
		for (Contact con : p_newRecords) {
			if (con.AccountId != null) {
				accountIds.add(con.AccountId);
			}
		}
		
		Map<Id, Account> accountMap = new Map<Id, Account>([
			SELECT OwnerId 
			FROM Account 
			WHERE Id IN :accountIds 
			LIMIT 10000]);
		
		//Create related Case records for each valid Contact records.
		List<Case> relatedCases = new List<Case>();
		for (Contact con : p_newRecords) {
			relatedCases.add(
				new Case(
					ContactId = con.Id, 
					OwnerId = (accountMap.containsKey(con.AccountId)) 
							? accountMap.get(con.AccountId).OwnerId 
							: UserInfo.getUserId(), 
					AccountId = con.AccountId, 
					Status = 'Working', 
					Origin = 'New Contact', 
					Priority = PRIORITY_LEVEL.get(con.Level__c)
				));
		}
		
		insert relatedCases;
		
		List<Case> selectedCases = [
			SELECT CaseNumber, Contact.Name, Contact.OwnerId, Priority 
			FROM Case 
			WHERE Id IN :new Map<Id, Case>(relatedCases).keySet() 
			LIMIT 10000];
		
		//Create Task records for each valid Case records.
		List<Task> tasks = new List<Task>();
		for (Case newCase : selectedCases) {
			tasks.add(
				new Task(
					WhatId = newCase.Id, 
					Subject = 'Welcome call for ' + newCase.Contact.Name + ' - ' + newCase.CaseNumber, 
					ActivityDate = ACTIVITY_DATE.get(newCase.Priority), 
					OwnerId = newCase.Contact.OwnerId 
				));
		}
		
		insert tasks;
	}
}