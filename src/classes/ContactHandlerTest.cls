/**
* @description Test ContactHandler class. Create Case and assign to Contact.
*/
@isTest
private class ContactHandlerTest {
	@isTest
	/**
	* @description Test creating case and assigning it to Contact.
	*/
	private static void testAssignCase() {
		User accountUser = new User(
			FirstName = 'Test', 
			LastName = 'User', 
			Email = 'account@email.com', 
			UserName = 'account@username.com', 
			ProfileId = UserInfo.getProfileId(), 
			Alias = 'userAcc', 
			CommunityNickName = 'userAcc', 
			TimeZoneSidKey = 'America/New_York', 
			LocaleSidKey = 'en_US', 
			EmailEncodingKey = 'ISO-8859-1', 
			LanguageLocaleKey = 'en_US');
		insert accountUser;
		
		Account account = new Account(Name = 'Grand Hotels', OwnerId = accountUser.Id);
		insert account;
		
		User testUser = new User(
			FirstName = 'Test', 
			LastName = 'User', 
			Email = 'test@email.com', 
			UserName = 'test@username.com', 
			ProfileId = UserInfo.getProfileId(), 
			Alias = 'user1', 
			CommunityNickName = 'user1', 
			TimeZoneSidKey = 'America/New_York', 
			LocaleSidKey = 'en_US', 
			EmailEncodingKey = 'ISO-8859-1', 
			LanguageLocaleKey = 'en_US');
		insert testUser;
		
		Test.startTest();
		
		List<Contact> contacts = new List<Contact> {
			new Contact(
				FirstName = 'Jhon', 
				LastName = 'Bond', 
				AccountId = account.Id, 
				Level__c = 'Primary', 
				OwnerId = UserInfo.getUserId()), 
			new Contact(
				FirstName = 'Jane', 
				LastName = 'Grey', 
				AccountId = account.Id, 
				Level__c = 'Secondary', 
				OwnerId = UserInfo.getUserId()), 
			new Contact(
				FirstName = 'Babara', 
				LastName = 'Levy', 
				AccountId = account.Id, 
				Level__c = 'Tertiary', 
				OwnerId = testUser.Id)
		};
		insert contacts;
		
		Test.stopTest();
		
		List<Case> insertedCases = [
			SELECT OwnerId, AccountId, Status, Origin, Priority, Contact.OwnerId, Contact.AccountId, 
				Contact.Level__c, Contact.Name, CaseNumber, 
				(SELECT Subject, ActivityDate, OwnerId 
				FROM Tasks) 
			FROM Case 
			WHERE ContactId IN :new Map<Id, Contact>(contacts).keySet()
			LIMIT 4];
		
		for (Case c : insertedCases) {
			System.assertEquals(accountUser.Id, c.OwnerId, 'Invalid Case OwnerId value.');
			System.assertEquals(c.Contact.AccountId, c.AccountId, 'Invalid Case AccountID value.');
			System.assertEquals('Working', c.Status, 'Invalid Case Status value.');
			System.assertEquals('New Contact', c.Origin, 'Invalid Case Subject value.');
			System.assertEquals(ContactHandler.PRIORITY_LEVEL.get(c.Contact.Level__c), c.Priority, 
					'Invalid Case Priority value.');
					
			System.assert( ! c.Tasks.isEmpty());
			Task t = c.Tasks.get(0);
			
			System.assertEquals('Welcome call for ' + c.Contact.Name + ' - ' + c.CaseNumber, t.Subject, 
					'Invalid Task Subject value.');
			System.assertEquals(ContactHandler.ACTIVITY_DATE.get(c.Priority), t.ActivityDate, 
					'Invalid Task ActivityDate value.');
			System.assertEquals(c.Contact.OwnerId, t.OwnerId, 'Invalid Task OwnerId value.');
		}
	}
}