trigger Contact on Contact (after delete, after insert, after undelete, after update, before delete, 
		before insert, before update) {
	
	if (ContactHandler.enablesTrigger) {
		if (Trigger.isAfter && Trigger.isInsert) {
			ContactHandler.attachCase(Trigger.new);
		}
	}
}